package com.parikshit.ojass.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import com.parikshit.ojass.Activity.CoreTeamActivity;
import com.parikshit.ojass.Activity.DevelopersActivity;
import com.parikshit.ojass.Activity.EventsActivity;
import com.parikshit.ojass.Activity.FAQActivity;
import com.parikshit.ojass.Activity.FeedActivity;
import com.parikshit.ojass.Activity.GuruGyanActivity;
import com.parikshit.ojass.Activity.MapActivity;
import com.parikshit.ojass.Activity.SponsersActivity;
import com.parikshit.ojass.Activity.Sponsor_new;
import com.parikshit.ojass.R;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.parikshit.ojass.Widgets.DrawerRow;

import java.util.Collections;
import java.util.List;


public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.MyViewHolder> {
    private LayoutInflater inflator;
    private List<DrawerRow> rowData = Collections.emptyList();
    private DrawerLayout drawerLayout;
    private Context context;
    private ProgressDialog p;

    public DrawerAdapter(DrawerLayout drawerLayout, Context context, List<DrawerRow> drawerData) {
        inflator = LayoutInflater.from(context);
        rowData = drawerData;
        this.drawerLayout = drawerLayout;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflator.inflate(R.layout.drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position) {

        DrawerRow current = rowData.get(position);
        viewHolder.title.setText(current.title);
        viewHolder.icon.setImageResource(current.iconId);
//        viewHolder.itemView.setTag(rowData.get(position).categoryId);
    }


    @Override
    public int getItemCount() {
        return rowData.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.listText);
            icon = (ImageView) itemView.findViewById(R.id.listIcon);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Intent drawerActivity = null;
            if (getAdapterPosition() == 0) {
            } else if (getAdapterPosition() == 1) {
                drawerActivity = new Intent(context, EventsActivity.class);
                context.startActivity(drawerActivity);
            } else if (getAdapterPosition() == 2) {
                drawerActivity = new Intent(context, FeedActivity.class);
                context.startActivity(drawerActivity);
            } else if (getAdapterPosition() == 3) {

                drawerActivity = new Intent(context, GuruGyanActivity.class);
                context.startActivity(drawerActivity);


               // drawerActivity = new Intent(context, ScheduleLayoutActivity.class);
               // context.startActivity(drawerActivity);
            } else if (getAdapterPosition() == 4) {
                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setMessage("Will be published later");
                builder.setNegativeButton("CLOSE",null);
                builder.setCancelable(false);
                builder.setTitle("Schedule");
                AlertDialog alertDialog=builder.create();
                alertDialog.show();
            } else if (getAdapterPosition() == 5) {
                drawerActivity = new Intent(context, CoreTeamActivity.class);
                context.startActivity(drawerActivity);
            } else if (getAdapterPosition() == 6) {
                drawerActivity = new Intent(context, MapActivity.class);
                context.startActivity(drawerActivity);
            } else if (getAdapterPosition() == 7) {
                drawerActivity = new Intent(context, FAQActivity.class);
                context.startActivity(drawerActivity);
            }else if (getAdapterPosition() == 8) {
                drawerActivity = new Intent(context, DevelopersActivity.class);
                context.startActivity(drawerActivity);
            }else if (getAdapterPosition() == 9) {
                drawerActivity = new Intent(context, Sponsor_new.class);
                context.startActivity(drawerActivity);
            }
            drawerLayout.closeDrawers();
        }
    }

}
