package com.parikshit.ojass.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gigamole.infinitecycleviewpager.VerticalInfiniteCycleViewPager;
import com.parikshit.ojass.R;
import com.parikshit.ojass.Utils.UtilsCoreTeam;

import static com.parikshit.ojass.Utils.UtilsCoreTeam.setupItem;


/**
 * Created by GIGAMOLE on 7/27/16.
 */
public class HorizontalPagerAdapter extends PagerAdapter {

    public  TextView textViewPost;

//    private final Utils.LibraryObject[] LIBRARIES = new Utils.LibraryObject[]{
//            new Utils.LibraryObject(
//                    "POST 1",
//                    R.drawable.ic_strategy,
//                    "Strategy"
//            ),
//            new Utils.LibraryObject(
//                    "POST 2",
//                    R.drawable.ic_design,
//                    "Design"
//            ),
//            new Utils.LibraryObject(
//                    "POST 3",
//                    R.drawable.ic_development,
//                    "Development"
//            ),
//
//    };

    private UtilsCoreTeam.LibraryObject LIBRARIES[][] = new UtilsCoreTeam.LibraryObject[11][6];

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private boolean mIsTwoWay;
    private ViewPager.OnPageChangeListener pageChangeListener;

    public HorizontalPagerAdapter(final Context context, final boolean isTwoWay) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        LIBRARIES[0] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Saurav Singh",
                        R.drawable.saurav_techsec,
                        "Technical Secretary",
                        "9122981287"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Amir Khan",
                        R.drawable.aamir_khan,
                        "Joint Secretary",
                        "7209780930"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Siddharth Shanker Anand",
                        R.drawable.sid,
                        "General Secretary",
                        "7870363708"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "PD Chaudhary",
                        R.drawable.pd,
                        "Treasurer",
                        "8252649320"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Korada Bhargav",
                        R.drawable.korada,
                        "SpokesPerson",
                        "8969189356"
                ),

        };

        LIBRARIES[1] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Praveer Varun Sharma",
                        R.drawable.praveer,
                        " ",
                        "7762827779"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Parikshit Chadha",
                        R.drawable.parikshit,
                        " ",
                        "8051137587"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Mayank Agarwal",
                        R.drawable.mayank_new,
                        " ",
                        "8102502220"
                ),

        };

        LIBRARIES[2] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Vishal Khare",
                        R.drawable.vishalkhare,
                        " ",
                        "7319706481"

                ),
                new UtilsCoreTeam.LibraryObject(
                        "Ankita Kumari",
                        R.drawable.ankitaca,
                        " ",
                        "7763811266"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Vivek Vejandla",
                        R.drawable.vvivek,
                        " ",
                        "8978865565"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Anwesha Chakraborty",
                        R.drawable.anwesha,
                        " ",
                        "8102593903"
                ),
        };
        LIBRARIES[3] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Siddharth Sanghvi",
                        R.drawable.sanghvi,
                        " ",
                        "9304654686"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Raj Kothari",
                        R.drawable.kothari,
                        " ",
                        "9420903156"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Katiki Nikhil",
                        R.drawable.nikhil,
                        " ",
                        "9430165010"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Neha Saw",
                        R.drawable.neha,
                        " ",
                        "9006755900"
                ),
        };
        LIBRARIES[4] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Akshay Tyagi",
                        R.drawable.akshay_tyagi,
                        " ",
                        "7209530361"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Kumar Ankur",
                        R.drawable.ankur,
                        " ",
                        "7631167282"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Meka Rahul",
                        R.drawable.rahul,
                        " ",
                        "8235367127"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Shetu",
                        R.drawable.shetu,
                        " ",
                        "7782027199"
                ),

        };
        LIBRARIES[5] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Himanshu Sharma",
                        R.drawable.himanshu_core,
                        " ",
                        "7209387418"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Aditya Ghodela",
                        R.drawable.aditya,
                        " ",
                        "8952925333"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "K Harshavardhana",
                        R.drawable.kharshvardhan,
                        " ",
                        "9430164998"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Durgesh",
                        R.drawable.durgesh,
                        " ",
                        "7070450640"
                ),


        };
        LIBRARIES[6] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Ravi Roshan",
                        R.drawable.ravi,
                        " ",
                        "7549405304"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Deepak Kumar",
                        R.drawable.deepak,
                        " ",
                        "7870357237"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Abhishek Shahi",
                        R.drawable.shahi,
                        " ",
                        "7870528490"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Meghana Priya",
                        R.drawable.meghana,
                        " ",
                        "9031000639"
                ),
        };
        LIBRARIES[7] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Suraj Potnuru",
                        R.drawable.suraj,
                        " ",
                        "8292980033"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Shubham Singh",
                        R.drawable.shubham,
                        " ",
                        "8873856624"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Mukesh Kumar",
                        R.drawable.mukesh,
                        " ",
                        "9472706374"

                ),
                new UtilsCoreTeam.LibraryObject(
                        "Rishav Sagar",
                        R.drawable.rishav,
                        " ",
                        "9546531225"
                ),

        };
        LIBRARIES[8] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Naveen Sai Kiran",
                        R.drawable.naveen,
                        " ",
                        "9959232139"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Seshu",
                        R.drawable.seshu,
                        " ",
                        "9430164996"
                ),

                new UtilsCoreTeam.LibraryObject(
                        "I S Venkat Tarun",
                        R.drawable.istarun,
                        " ",
                        "7762828292"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Satyam Chaubey",
                        R.drawable.satyam,
                        " ",
                        "8051057553"
                ),

        };
        LIBRARIES[9] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "Amritesh Ranjan",
                        R.drawable.amritesh,
                        " ",
                        "9608667456"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Aman Kumar",
                        R.drawable.aman,
                        " ",
                        "8541918655"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Sourabh Sharma",
                        R.drawable.sourabh,
                        " ",
                        "7209824669"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Amit Kumar",
                        R.drawable.amit,
                        " ",
                        "9162659555"
                ),

        };
        LIBRARIES[10] = new UtilsCoreTeam.LibraryObject[]{
                new UtilsCoreTeam.LibraryObject(
                        "V S Pratibha",
                        R.drawable.pratibha,
                        " ",
                        "8235364241"
                ),
                new UtilsCoreTeam.LibraryObject(
                        "Ankita Gupta",
                        R.drawable.ankita,
                        " ",
                        "9973652350"
                ),
        };

                mIsTwoWay = isTwoWay;
    }

    @Override
    public int getCount() {
        return mIsTwoWay ? 11: LIBRARIES.length;
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view;
        if (mIsTwoWay) {
            view = mLayoutInflater.inflate(R.layout.two_way_item, container, false);


            //setting Post to the textView
         textViewPost = (TextView) view.findViewById(R.id.textViewPost);

            final VerticalInfiniteCycleViewPager verticalInfiniteCycleViewPager =
                    (VerticalInfiniteCycleViewPager) view.findViewById(R.id.vicvp);
            verticalInfiniteCycleViewPager.setAdapter(
                    new VerticalPagerAdapter(LIBRARIES[position],mContext)
            );
            verticalInfiniteCycleViewPager.setCurrentItem(position);

        } else {

            view = mLayoutInflater.inflate(R.layout.core_team_item, container, false);
            setupItem(mContext, view, LIBRARIES[0][position]);

        }

        container.addView(view);
        return view;
    }



    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }


}
