package com.parikshit.ojass.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parikshit.ojass.R;
import com.parikshit.ojass.Utils.Utils;

import static com.parikshit.ojass.Utils.Utils.setupItem;


/**
 * Created by mayank2212 on 1/11/2017.
 */

public class OnboardingAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private boolean mIsTwoWay;

    private final Utils.LibraryObject[] LIBRARIES = new Utils.LibraryObject[]{
            new Utils.LibraryObject(
                    R.drawable.screen1,
                    ""
            ),
            new Utils.LibraryObject(
                    R.drawable.screen2,
                    ""
            ),
            new Utils.LibraryObject(
                    R.drawable.screen3,
                    ""
            ),
            new Utils.LibraryObject(
                    R.drawable.screen4,
                    ""
            )
    };


    public OnboardingAdapter(final Context context, final boolean isTwoWay) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mIsTwoWay = isTwoWay;
    }

    @Override
    public int getCount() {
        return 5 ;
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view;
        if(position<4) {
            view = mLayoutInflater.inflate(R.layout.item, container, false);
            setupItem(view, LIBRARIES[position]);
            container.addView(view);
        }
        else
        {
            view = mLayoutInflater.inflate(R.layout.item, container, false);
        }
        return view;
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }
}