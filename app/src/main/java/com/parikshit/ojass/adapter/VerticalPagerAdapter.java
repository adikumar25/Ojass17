package com.parikshit.ojass.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parikshit.ojass.R;
import com.parikshit.ojass.Utils.UtilsCoreTeam;


import static com.parikshit.ojass.Utils.UtilsCoreTeam.setupItem;

/**
 * Created by GIGAMOLE on 7/27/16.
 */
public class VerticalPagerAdapter extends PagerAdapter {

    private  Context context;
    private TextView textViewPost;
    private UtilsCoreTeam.LibraryObject[] TWO_WAY_LIBRARIES;


//    private final Utils.LibraryObject[] TWO_WAY_LIBRARIES = new Utils.LibraryObject[]{
//            new Utils.LibraryObject(
//                    "POST 1",
//                    R.drawable.profile_pic,
//                    "Manish Kumar1"
//            ),
//            new Utils.LibraryObject(
//                    "POST 2",
//                    R.drawable.profile_pic,
//                    "Manish Kumar2"
//            ),
//            new Utils.LibraryObject(
//                    "POST 3",
//                    R.drawable.profile_pic,
//                    "Manish Kumar3"
//            ),
//
//    };

    private LayoutInflater mLayoutInflater;

    public VerticalPagerAdapter(UtilsCoreTeam.LibraryObject TWO_WAY_LIBRARIES[],final Context context) {
        mLayoutInflater = LayoutInflater.from(context);
        this.TWO_WAY_LIBRARIES=TWO_WAY_LIBRARIES;
        this.context = context;


    }


    @Override
    public int getCount() {
        return TWO_WAY_LIBRARIES.length;
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view = mLayoutInflater.inflate(R.layout.core_team_item, container, false);

        setupItem(context, view, TWO_WAY_LIBRARIES[position]);

        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }
}
