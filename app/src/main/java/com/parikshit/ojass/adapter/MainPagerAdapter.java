package com.parikshit.ojass.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.parikshit.ojass.Fragments.TwoWayPagerFragment;


/**
 * Created by GIGAMOLE on 8/18/16.
 */
public class MainPagerAdapter extends FragmentStatePagerAdapter {

    private final static int COUNT = 3;
    private TwoWayPagerFragment pagerFragment;



    public MainPagerAdapter(final FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(final int position) {

       return new TwoWayPagerFragment();
    }

    @Override
    public int getCount() {
        return COUNT;
    }
}
