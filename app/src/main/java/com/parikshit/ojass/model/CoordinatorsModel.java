package com.parikshit.ojass.model;


public class CoordinatorsModel {


    String name;
    String phone;


    public CoordinatorsModel(String name, String phone) {

        this.name = name;
        this.phone=phone;

    }
    public CoordinatorsModel()
    {

    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }


}
