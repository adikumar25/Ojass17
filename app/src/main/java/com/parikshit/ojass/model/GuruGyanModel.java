package com.parikshit.ojass.model;

/**
 * Created by Aditya on 12-02-2017.
 */

public class GuruGyanModel {
    String desc,imgurl,date;

    public String getDate() {
        return date;
    }

    public GuruGyanModel(String desc, String imgurl, String date) {
        this.desc = desc;
        this.imgurl = imgurl;
        this.date=date;

    }

    public GuruGyanModel() {

    }

    public String getImgurl() {
        return imgurl;
    }

    public String getDesc() {
        return desc;
    }
}
