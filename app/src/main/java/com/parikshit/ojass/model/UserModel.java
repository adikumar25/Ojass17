package com.parikshit.ojass.model;

/**
 * Created by Aditya on 09-02-2017.
 */

public class UserModel {
    String name, roll, branch, institute, photoURL, email;

    public UserModel(String email, String photoURL, String institute, String branch, String roll, String name) {

        this.email = email;
        this.photoURL = photoURL;
        this.institute = institute;
        this.branch = branch;
        this.roll = roll;
        this.name = name;

    }

    public UserModel() {

    }


    public String getName() {
        return name;
    }

    public String getRoll() {
        return roll;
    }

    public String getBranch() {
        return branch;
    }

    public String getInstitute() {
        return institute;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public String getEmail() {
        return email;
    }
}
