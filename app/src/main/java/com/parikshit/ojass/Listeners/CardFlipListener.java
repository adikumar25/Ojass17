package com.parikshit.ojass.Listeners;

/**
 * Created by rishavz_sagar on 19-Dec-16.
 */

public interface CardFlipListener {
    public void onCardFlipEnd();
    public void onCardFlipStart();
}