package com.parikshit.ojass.Widgets;

/**
 * Created by rishavz_sagar on 02-Feb-17.
 */

public interface ViewPageChangedListener {
    void onHorizontalPageChanged(boolean increase);
}
