package com.parikshit.ojass.Widgets;

/**
 * Created by rishavz_sagar on 19-Dec-16.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.parikshit.ojass.Listeners.CardFlipListener;
import com.parikshit.ojass.R;
import com.squareup.picasso.Picasso;


public class Event_Cards extends CardView implements Animator.AnimatorListener{

    public enum Corner {
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT
    }
    private final int CAMERA_DISTANCE = 8000;
    private final int MIN_FLIP_DURATION = 300;
    private final int VELOCITY_TO_DURATION_CONSTANT = 15;
    private final int MAX_FLIP_DURATION = 700;
    private final int ROTATION_PER_CARD = 2;
    private final int ROTATION_DELAY_PER_CARD = 50;
    private final int ROTATION_DURATION = 2000;
    private final int ANTIALIAS_BORDER = 1;
    private boolean mIsFrontShowing = true;
    private boolean mIsHorizontallyFlipped = false;
    private Matrix mHorizontalFlipMatrix;
    private CardFlipListener mCardFlipListener;
    private String eventName,imgUrl,date;
    private Context context;
    private Camera camera;
    private RelativeLayout eventFrontLayout,eventBackLayout;
    private CustomFontTextView name,dat;
    private ImageView imageView;

    public Event_Cards(Context context,String eventName,String imgUrl,String date) {
        super(context);
        this.context=context;
        this.eventName=eventName;
        this.imgUrl=imgUrl;
        this.date=date;
        init(context);
    }
    public Event_Cards(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }
    /** Loads the bitmap drawables used for the front and back for this card.*/
    public void init(Context context) {
        camera = new Camera();
        mHorizontalFlipMatrix = new Matrix();
        setCameraDistance(CAMERA_DISTANCE);
        setRadius(0.1f);
        loadLayouts();
        updateEventCardLayout();
    }

    private void loadLayouts() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        eventFrontLayout = (RelativeLayout) inflater.inflate(R.layout.event_layout_front, null);
        eventBackLayout=(RelativeLayout) inflater.inflate(R.layout.event_layout_back, null);
        name=(CustomFontTextView)eventFrontLayout.findViewById(R.id.name);
        imageView=(ImageView) eventFrontLayout.findViewById(R.id.image_guru);
        dat=(CustomFontTextView) eventFrontLayout.findViewById(R.id.date);
        name.setText(eventName);
        dat.setText(date);
        Picasso.with(context).load(imgUrl).placeholder(R.drawable.default_user).into(imageView);
    }
    /** Initiates a horizontal flip from right to left. */
    public void flipRightToLeft(int numberInPile, int velocity) {
        setPivotX(0);
        flipHorizontally(numberInPile, false, velocity);
    }
    /** Initiates a horizontal flip from left to right. */
    public void flipLeftToRight(int numberInPile, int velocity) {
        setPivotX(getWidth());
        flipHorizontally(numberInPile, true, velocity);
    }
    public void flipHorizontally (int numberInPile, boolean clockwise, int velocity) {
        toggleFrontShowing();
        PropertyValuesHolder rotation = PropertyValuesHolder.ofFloat(View.ROTATION_Y,
                clockwise ? 180 : -180);
        PropertyValuesHolder xOffset = PropertyValuesHolder.ofFloat(View.TRANSLATION_X,
                2*numberInPile );
        PropertyValuesHolder yOffset = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y,
                2*numberInPile );
        ObjectAnimator cardAnimator = ObjectAnimator.ofPropertyValuesHolder(this, rotation,
                xOffset, yOffset);
        cardAnimator.addListener(this);
        cardAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (valueAnimator.getAnimatedFraction() >= 0.5) {
                    updateEventCardLayout();
                }
            }
        });
        Keyframe shadowKeyFrameStart = Keyframe.ofFloat(0, 0);
        Keyframe shadowKeyFrameMid = Keyframe.ofFloat(0.5f, 1);
        Keyframe shadowKeyFrameEnd = Keyframe.ofFloat(1, 0);
        PropertyValuesHolder shadowPropertyValuesHolder = PropertyValuesHolder.ofKeyframe
                ("shadow", shadowKeyFrameStart, shadowKeyFrameMid, shadowKeyFrameEnd);
        ObjectAnimator colorizer = ObjectAnimator.ofPropertyValuesHolder(this,
                shadowPropertyValuesHolder);
        colorizer.addListener(this);
        mCardFlipListener.onCardFlipStart();
        AnimatorSet set = new AnimatorSet();
        int duration = MAX_FLIP_DURATION - Math.abs(velocity) / VELOCITY_TO_DURATION_CONSTANT;
        duration = duration < MIN_FLIP_DURATION ? MIN_FLIP_DURATION : duration;
        set.setDuration(duration);
        set.playTogether(cardAnimator, colorizer);
        set.setInterpolator(new LinearInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                toggleIsHorizontallyFlipped();
                updateEventCardLayout();
                updateLayoutParams();
                mCardFlipListener.onCardFlipEnd();
            }
        });
        set.start();
    }
    public void toggleFrontShowing() {
        mIsFrontShowing = !mIsFrontShowing;
    }
    public void toggleIsHorizontallyFlipped() {
        mIsHorizontallyFlipped = !mIsHorizontallyFlipped;
        invalidate();
    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mHorizontalFlipMatrix.setScale(-1, 1, w / 2, h / 2);
    }
    /**
     *  Scale the canvas horizontally about its midpoint in the case that the card
     *  is in a horizontally flipped state.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        if (mIsHorizontallyFlipped) {
            canvas.concat(mHorizontalFlipMatrix);
        }
        super.onDraw(canvas);
    }
    /**
     *  Updates the layout parameters of this view so as to reset the rotationX and
     *  rotationY parameters, and remain independent of its previous position, while
     *  also maintaining its current position in the layout.
     */
    public void updateLayoutParams () {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
        params.leftMargin = (int)(params.leftMargin + ((Math.abs(getRotationY()) % 360) / 180) *
                (2 * getPivotX () - getWidth()));
        setRotationX(0);
        setRotationY(0);
        setLayoutParams(params);
    }
    /**
     * Toggles the visible bitmap of this view between its front and back drawables
     * respectively.
     */
    public void updateEventCardLayout() {
        RelativeLayout currentLayout = mIsFrontShowing ? eventFrontLayout: eventBackLayout;
        removeAllViews();
        addView(currentLayout);
        setRadius(10);
    }
    /**
     * Sets the appropriate translation of this card depending on how many cards
     * are in the pile underneath it.
     */
    public void updateTranslation (int numInPile) {
        setTranslationX( 2*numInPile);
        setTranslationY( 2*numInPile);
    }

    public void setCardFlipListener(CardFlipListener cardFlipListener) {
        mCardFlipListener = cardFlipListener;
    }

    @Override
    public void onAnimationStart(Animator animation) {
        setEnabled(false);
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        setEnabled(true);
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        setEnabled(true);
    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}