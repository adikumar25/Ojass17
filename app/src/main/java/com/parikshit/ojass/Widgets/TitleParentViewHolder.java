package com.parikshit.ojass.Widgets;

import android.view.View;
import android.widget.TextView;


import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.parikshit.ojass.R;


/**
 * Created by admin on 19-01-2017.
 */

public class TitleParentViewHolder extends ParentViewHolder {

    public TextView _textView;
    public TitleParentViewHolder(View itemView) {
        super(itemView);
        _textView=(TextView)itemView.findViewById(R.id.parentTitle);
    }
}
