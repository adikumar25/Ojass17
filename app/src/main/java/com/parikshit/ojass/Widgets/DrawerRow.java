package com.parikshit.ojass.Widgets;


public class DrawerRow {
    public int iconId;
    public String title;

    public DrawerRow(int iconId, String title) {
        this.iconId = iconId;
        this.title = title;
    }
}
