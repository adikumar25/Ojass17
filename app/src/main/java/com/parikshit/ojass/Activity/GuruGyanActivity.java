package com.parikshit.ojass.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import com.parikshit.ojass.Listeners.CardFlipListener;
import com.parikshit.ojass.R;
import com.parikshit.ojass.Widgets.Event_Cards;
import com.parikshit.ojass.model.GuruGyanModel;

import java.util.ArrayList;
import java.util.List;




public class GuruGyanActivity extends Activity implements CardFlipListener {



    static Long STARTING_NUMBER_CARDS=MainActivity.number;
    final static int RIGHT_STACK = 0;
    final static int LEFT_STACK = 1;
    int mCardHeight = 0;
    int mLeftCardWidth=0;
    int mRightCardWidth=0;
    int mVerticalPadding;
    int mHorizontalPadding;
    static boolean mTouchEventsEnabled = true;
    boolean[] mIsStackEnabled;
    RelativeLayout mLayout;
    List<ArrayList<Event_Cards>> mStackCards;
    private Context context;
    static String tag;
    float mDownX,mDownY;
    boolean isClicked;
    public ArrayList<GuruGyanModel> data;
    private ProgressDialog p;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guru_gyan);
        context=getApplicationContext();
        mStackCards = new ArrayList<ArrayList<Event_Cards>>();
        mStackCards.add(new ArrayList<Event_Cards>());
        mStackCards.add(new ArrayList<Event_Cards>());
        mIsStackEnabled = new boolean[2];
        mIsStackEnabled[0] = true;
        mIsStackEnabled[1] = true;
        mVerticalPadding = 30;
        mHorizontalPadding = 30;
        data=new ArrayList<>();




        mLayout = (RelativeLayout)findViewById(R.id.events_layout);
        ViewTreeObserver observer = mLayout.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    mLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                mCardHeight = mLayout.getHeight()-150;
                mLeftCardWidth=80;
                mRightCardWidth=mLayout.getWidth()-120;
                for (int x = 0; x < STARTING_NUMBER_CARDS; x++) {
                    //String eventName="Event "+x;
                    addNewCard(RIGHT_STACK,MainActivity.data1.get(x).getDesc(),MainActivity.data1.get(x).getImgurl(),MainActivity.data1.get(x).getDate());
                }
            }
        });

    }

    public void addNewCard(int stack,String eventName,String imgUrl,String date) {
        Event_Cards view = new Event_Cards(this,eventName,imgUrl,date);
        view.setTag(mStackCards.get(stack).size()+"");
        view.updateTranslation(mStackCards.get(stack).size());
        if(mStackCards.get(LEFT_STACK).size()<2) {
            view.setCardFlipListener(this);

            view.setOnTouchListener(
                    new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (mTouchEventsEnabled) {
                                tag = v.getTag() + "";
                                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                                    case MotionEvent.ACTION_DOWN:
                                        mDownX = event.getX();
                                        mDownY = event.getY();
                                        isClicked = true;
                                        break;
                                    case MotionEvent.ACTION_CANCEL:
                                    case MotionEvent.ACTION_UP:
                                        if (isClicked && Math.abs(event.getX() - mDownX) < 10 && Math.abs(event.getY() - mDownY) < 10)
                                         //   showDetails();
                                        break;
                                    case MotionEvent.ACTION_MOVE:
                                        if (isClicked && Math.abs(event.getX() - mDownX) >= 10 && Math.abs(event.getY() - mDownY) >= 10) {
                                            if ((event.getX() - mDownX) > 0)
                                                swipeCard(0);
                                            else
                                                swipeCard(1);
                                        }
                                        break;
                                }
                                return true;
                            } else
                                return false;
                        }
                    }
            );
        }
        view.setPadding(mHorizontalPadding, mVerticalPadding, mHorizontalPadding, mVerticalPadding);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((stack == RIGHT_STACK ? mRightCardWidth: mLeftCardWidth),
                mCardHeight);
        params.topMargin = 50;
        params.rightMargin=20;
        params.leftMargin=10;
        params.leftMargin = (stack == RIGHT_STACK ? mLeftCardWidth: 0);
        mStackCards.get(stack).add(view);
        mLayout.addView(view, params);
    }

   /* private void showDetails() {
        Intent event=new Intent(context,EventsActivity.class);
        Log.d("MainActivity",tag);
        event.putExtra("eventName",tag);
        startActivity(event);
    }*/

    private void swipeCard(int x) {
        int stack = getStack(x);
        ArrayList<Event_Cards> cardStack = mStackCards.get(stack);
        int size = cardStack.size();
        if (size > 0) {
            rotateCardView(cardStack.get(size - 1), stack, 10);
        }
    }

    public void rotateCardView(final Event_Cards cardView, int stack, float velocityX) {
        boolean bothStacksEnabled = mIsStackEnabled[RIGHT_STACK] && mIsStackEnabled[LEFT_STACK];
        ArrayList<Event_Cards>leftStack = mStackCards.get(LEFT_STACK);
        ArrayList<Event_Cards>rightStack = mStackCards.get(RIGHT_STACK);
        switch (stack) {
            case RIGHT_STACK:
                    if (!bothStacksEnabled) {
                        break;
                    }
                    mLayout.bringChildToFront(cardView);
                    mLayout.requestLayout();
                    rightStack.remove(rightStack.size() - 1);
                    leftStack.add(cardView);
                    cardView.flipRightToLeft(leftStack.size() - 1, (int)velocityX);
                break;
            case LEFT_STACK:
                    if (!bothStacksEnabled) {
                        break;
                    }
                    mLayout.bringChildToFront(cardView);
                    mLayout.requestLayout();
                    leftStack.remove(leftStack.size() - 1);
                    rightStack.add(cardView);
                    cardView.flipLeftToRight(rightStack.size() - 1, (int)velocityX);
                break;
            default:
                break;
        }
    }

    public int getStack(int x) {
        boolean isLeft = (x ==0);
        return isLeft ? LEFT_STACK : RIGHT_STACK;
    }


    @Override
    public void onCardFlipEnd() {
        mTouchEventsEnabled = true;
    }
    @Override
    public void onCardFlipStart() {
        mTouchEventsEnabled = false;
    }
}
