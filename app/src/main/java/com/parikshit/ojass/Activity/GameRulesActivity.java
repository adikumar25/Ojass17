package com.parikshit.ojass.Activity;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.parikshit.ojass.R;

import java.util.EnumMap;
import java.util.Map;

public class GameRulesActivity extends AppCompatActivity{

    private TextView t1, t2, t3;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_rules);
        auth=FirebaseAuth.getInstance();
        String data=auth.getCurrentUser().getUid();
        t1 = (TextView) findViewById(R.id.title_game_rules);
        t2 = (TextView) findViewById(R.id.text_game_rules);
        t3 = (TextView) findViewById(R.id.title_toolbar_game);

        Typeface font1 = Typeface.createFromAsset(getAssets(),"Ojass.otf");
        t1.setTypeface(font1);

        Typeface font2 = Typeface.createFromAsset(getAssets(), "game_content.ttf");
        t2.setTypeface(font2);

        Typeface font3 = Typeface.createFromAsset(getAssets(),"ToolbarText.ttf");
        t3.setTypeface(font3);
        String barcode_data =data;
        Bitmap bitmap = null;
        ImageView iv = (ImageView) findViewById(R.id.qr_code);

        try {

            bitmap = encodeAsBitmap(barcode_data, BarcodeFormat.QR_CODE, 300, 300);
            iv.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }

    }

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }


}
