package com.parikshit.ojass.Activity;

 import android.app.ProgressDialog;
 import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
 import android.widget.TextView;

 import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
 import com.google.firebase.database.DataSnapshot;
 import com.google.firebase.database.DatabaseError;
 import com.google.firebase.database.DatabaseReference;
 import com.google.firebase.database.FirebaseDatabase;
 import com.google.firebase.database.ValueEventListener;
 import com.parikshit.ojass.R;
import com.parikshit.ojass.adapter.FAQAdapter;
 import com.parikshit.ojass.model.FaqModel;
 import com.parikshit.ojass.model.TitleChild;
import com.parikshit.ojass.model.TitleCreater;
import com.parikshit.ojass.model.TitleParent;

import java.util.ArrayList;
import java.util.List;

public class FAQActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Toolbar toolbar;
    TextView toolbarText;
    FAQAdapter adapter;
    DatabaseReference ref;
    public static ArrayList<FaqModel> data;
    ProgressDialog p;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        recyclerView=(RecyclerView)findViewById(R.id.myRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        data=new ArrayList<>();
        ref=FirebaseDatabase.getInstance().getReference().child("faq");
        ref.keepSynced(true);

        p=new ProgressDialog(this);
        p.setMessage("Loading FAQs....");
        p.setCancelable(false);
        p.show();

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               //Toast.makeText(FAQActivity.this,"fetched"+dataSnapshot.getChildrenCount(),Toast.LENGTH_SHORT).show();
                p.dismiss();
                data.clear();
                for(DataSnapshot ds: dataSnapshot.getChildren())
                {
                    FaqModel q=ds.getValue(FaqModel.class);
                    data.add(q);
                   // Toast.makeText(FAQActivity.this,"Q"+q.getQuestion()+"\nA:"+q.getAns(),Toast.LENGTH_SHORT).show();

                }

                adapter = new FAQAdapter(FAQActivity.this,initData());
                adapter.setParentClickableViewAnimationDefaultDuration();
                adapter.setParentAndIconExpandOnClick(true);
                recyclerView.setAdapter(adapter);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        toolbar =(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbarText=(TextView)findViewById(R.id.toolbarText);
        toolbarText.setText("FAQ");
        Typeface typeface =Typeface.createFromAsset(getAssets(),"ToolbarText.ttf");
        toolbarText.setTypeface(typeface);








    }

    private List<ParentObject> initData() {
        TitleCreater titleCreater= new TitleCreater(FAQActivity.this);
        //titleCreater= TitleCreater.get(this);
        List<TitleParent> titles=TitleCreater._titleParents;
        List<ParentObject> parentObject = new ArrayList<>();
       // Toast.makeText(FAQActivity.this,"Title:"+titles.size(),Toast.LENGTH_SHORT).show();
        int i=0;
        for(TitleParent title:titles)
        {
            List<Object> childList = new ArrayList<>();
            //childList.add(new TitleChild(("It is LSE web style to title a page of FAQs 'Frequently asked questions (FAQs)'. While the abbreviation is in quite common usage this ensures that there can be no mistaking what they are")));
            childList.add(new TitleChild(data.get(i++).getAns()));
            title.setChildObjectList(childList);
            parentObject.add(title);
        }
        return parentObject;
    }
}