package com.parikshit.ojass.Activity;

import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.parikshit.ojass.R;
import com.parikshit.ojass.Widgets.DevCustomDialog;

public class DevelopersActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView i1, i2, i3, i4, i5, i6, i7, i8, i9, i10;
    public static int dev_index;
    private Toolbar dev_toolbar;
    TextView sponsers_toolbar;

    public static String[] dev_name = {"Aditya Kumar", "Roushan Kumar", "Vishnu Shubham", "Vivek Kr. Singh",
                                    "Mayank Agarwal", "Madhukar Rana", "Shivam Srivastava", "Risav Sagar",
                                    "Himanshu Sharma", "Manish Kumar"};

    public static int[] dev_image = {R.drawable.adi1, R.drawable.roushan, R.drawable.vishnu, R.drawable.vivek,
                                    R.drawable.mayank_new, R.drawable.madhukar, R.drawable.shivam,
                                    R.drawable.rishav, R.drawable.himanshu, R.drawable.manish};

    public static String[] dev_mob = {"7870517024", "9155956813", "8235335705", "8840806796",
                                    "8102502220", "9955177725", "9470918557", "9546531225", "8126769400",
                                        "8757555614"};

    public static String[] dev_email = {"adi.15j13@gmail.com", "roushankumar.nit@gmail.com", "vishnupakur@gmail.com",
                                        "vivek.singh0302@gmail.com", "mayankagarwal2212@gmail.com", "madhukarrana1997@gmail.com",
                                        "shivamsmarty28@gmail.com", "rishavsagar4b1@gmail.com", "kakadbest@gmail.com",
                                        "manishh776@gmail.com"};

    public static String[] linkedin = {"https://in.linkedin.com/in/aditya-k-a62b12a5",
            "https://www.linkedin.com/in/roushan-kumar-891200124",
            "https://www.linkedin.com/in/vishnu-shubham-a94093128",
            "https://in.linkedin.com/in/vivek-singh-a7174612a",
            "https://www.linkedin.com/in/mayank-agarwal-b0516ba6",
            "https://www.linkedin.com/in/madhukar-rana-28b045112",
            "https://www.linkedin.com/in/shivam-srivastava-70a644131",
            "https://in.linkedin.com/in/rishav-sagar-38558288",
            "https://www.linkedin.com/in/himanshu-sharma-a3218ba3",
            "https://www.linkedin.com/in/manish-kumar-800ab0112/"};

    public static String[] facebook = {"https://www.facebook.com/imcooladitya.kumar",
            "https://www.facebook.com/profile.php?id=100005336597333",
            "https://www.facebook.com/vishnushubham1",
            "https://www.facebook.com/profile.php?id=100007894937863",
            "https://www.facebook.com/search/top/?q=mayank%20agarwal",
            "https://www.facebook.com/madhukar.rana.96",
            "https://www.facebook.com/shivam.srivastava.1610",
            "https://www.facebook.com/rishavzsagar",
            "https://www.facebook.com/profile.php?id=100001262860799",
            "https://en-gb.facebook.com/people/Manish-Kumar/100002573260082"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developers);

        sponsers_toolbar = (TextView) findViewById(R.id.sponsers_toolbar_title);
        Typeface tf = Typeface.createFromAsset(getAssets(), "ToolbarText.ttf");
        sponsers_toolbar.setTypeface(tf);
        /// Changing the status bar color dynamically.................................................
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this , R.color.dev_statusbar_color));
        }

        i1 = (ImageView) findViewById(R.id.i1);
        i1.setImageResource(R.drawable.adi);
        i1.setOnClickListener(this);

        i2 = (ImageView) findViewById(R.id.i2);
        i2.setImageResource(R.drawable.roushan);
        i2.setOnClickListener(this);

        i3 = (ImageView) findViewById(R.id.i3);
        i3.setImageResource(R.drawable.vishnu);
        i3.setOnClickListener(this);

        i4 = (ImageView) findViewById(R.id.i4);
        i4.setImageResource(R.drawable.vivek);
        i4.setOnClickListener(this);

        i5 = (ImageView) findViewById(R.id.i5);
        i5.setImageResource(R.drawable.mayank_new);
        i5.setOnClickListener(this);

        i6 = (ImageView) findViewById(R.id.i6);
        i6.setImageResource(R.drawable.madhukar);
        i6.setOnClickListener(this);

        i7 = (ImageView) findViewById(R.id.i7);
        i7.setImageResource(R.drawable.shivam);
        i7.setOnClickListener(this);

        i8 = (ImageView) findViewById(R.id.i8);
        i8.setImageResource(R.drawable.rishav);
        i8.setOnClickListener(this);

        i9 = (ImageView) findViewById(R.id.i9);
        i9.setImageResource(R.drawable.himanshu);
        i9.setOnClickListener(this);

        i10 = (ImageView) findViewById(R.id.i10);
        i10.setImageResource(R.drawable.manish);
        i10.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.i1) {
            dev_index = 0;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        } else if(id == R.id.i2) {
            dev_index = 1;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        } else if(id == R.id.i3) {
            dev_index = 2;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        } else if(id == R.id.i4) {
            dev_index = 3;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        } else if(id == R.id.i5) {
            dev_index = 4;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        } else if(id == R.id.i6) {
            dev_index = 5;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        } else if(id == R.id.i7) {
            dev_index = 6;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        } else if(id == R.id.i8) {
            dev_index = 7;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        } else if(id == R.id.i9) {
            dev_index = 8;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        } else if(id == R.id.i10) {
            dev_index = 9;
            DevCustomDialog dialog = new DevCustomDialog();
            dialog.show(getFragmentManager(), "my_dialog");
        }
    }
}
