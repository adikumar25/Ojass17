package com.parikshit.ojass.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;

import com.parikshit.ojass.R;
import com.parikshit.ojass.adapter.PagerAdapter;
import com.rishavz_sagar.verticleviewpager.VerticalViewPager;

public class VerticalSwipeActivity extends AppCompatActivity implements View.OnTouchListener {

    VerticalViewPager verticalViewPager;
    Toolbar toolbar;
    private float x1, y1, x2, y2;
    private int Duration = 200;
    private long startTime;
    private final int MIN_DISTANCE = 180;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertical_swipe);

        verticalViewPager = (VerticalViewPager) findViewById(R.id.verticalViewPager);
        verticalViewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));

        toolbar = (Toolbar) findViewById(R.id.subEvent_des);
        setSupportActionBar(toolbar);
     //   verticalViewPager.setCurrentItem(1);

        verticalViewPager.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                // DOUBLE TAP -----------------------------------------------------------------------------

                break;
            case MotionEvent.ACTION_UP:
                startTime = System.currentTimeMillis();
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();

                float deltaX = x2 - x1;
                float deltaY = y2 - y1;
                if (deltaX > MIN_DISTANCE) {
                    // SWIPE LEFT TO RIGHT ---------------------------------------------------------
                        Intent intent = new Intent(this, ScrollViewActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        finish();
                } else if (Math.abs(deltaX) > MIN_DISTANCE) {
                    // SWIPE RIGHT TO LEFT --------------------------------------------------------
                    Intent intent = new Intent(this, HVSwipeActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    finish();

                } else if (deltaY > MIN_DISTANCE) {
                    // SWIPE TOP TO BOTTOM ---------------------------------------------------------

                } else if (Math.abs(deltaY) > MIN_DISTANCE) {
                    // SWIPE BOTTOM TO TOP ---------------------------------------------------------

                }
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
