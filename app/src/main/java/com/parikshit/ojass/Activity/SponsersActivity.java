package com.parikshit.ojass.Activity;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.parikshit.ojass.R;

public class SponsersActivity extends Activity {

    TextView sponsers_toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsers);

        sponsers_toolbar = (TextView) findViewById(R.id.sponsers_toolbar_title);
            Typeface tf = Typeface.createFromAsset(getAssets(), "ToolbarText.ttf");
            sponsers_toolbar.setTypeface(tf);

    }
}
