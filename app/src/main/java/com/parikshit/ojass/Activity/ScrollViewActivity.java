package com.parikshit.ojass.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

import com.parikshit.ojass.R;

public class ScrollViewActivity extends AppCompatActivity implements View.OnTouchListener {

    ScrollView scrollView;
    private float x1, y1, x2, y2;
    private int Duration = 200;
    private long startTime;
    private final int MIN_DISTANCE = 70;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_view);

        scrollView = (ScrollView) findViewById(R.id.scroll_view);
        scrollView.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent touchEvent) {
        switch(touchEvent.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                // DOUBLE TAP -----------------------------------------------------------------------------
                if(System.currentTimeMillis() - startTime <= Duration) {
                //    Toast.makeText(this, "DOUBLE TAP !!", Toast.LENGTH_SHORT).show();
                }
                break;

            case MotionEvent.ACTION_UP:
                startTime = System.currentTimeMillis();
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();

                float deltaX = x2 - x1;
                float deltaY = y2 - y1;
                if (deltaX > 250)
                {   // SWIPE LEFT TO RIGHT ---------------------------------------------------------
                //    Intent intent = new Intent(this, HVSwipeActivity.class);
                //    startActivity(intent);
                //    finish();
                }
                else if( Math.abs(deltaX) > 220)
                {    // SWIPE RIGHT TO LEFT --------------------------------------------------------
                    Intent intent = new Intent(this, VerticalSwipeActivity.class);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                    finish();
                //    Toast.makeText(this, "DOUBLE TAP !!", Toast.LENGTH_SHORT).show();
                }
                else if(deltaY > MIN_DISTANCE){
                    // SWIPE TOP TO BOTTOM ---------------------------------------------------------
                }
                else if( Math.abs(deltaY) > MIN_DISTANCE){
                    // SWIPE BOTTOM TO TOP ---------------------------------------------------------
                }
                break;
        }

        return false;
    }
}
