package com.parikshit.ojass.Activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.parikshit.ojass.R;
import com.parikshit.ojass.adapter.MainPagerAdapter;


public class CoreTeamActivity extends AppCompatActivity{

    private static String teamNames[]={"CREATIVE","ROBOTICS","MEDIA RELATIONS","CORE","PLANNING AND DEVELOPMENT ","CORPORATE AFFAIRS","PUBLIC RELATIONS","EVENT MANAGEMENT",
    "LOGISTICS","HOSPITALITY","WEB OPS"};
    private static int currentPos=0;
    private static TextView teamName;
    TextView ojassTeam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_core_team);
        ojassTeam=(TextView)findViewById(R.id.core_toolbar_title);
        Typeface tf = Typeface.createFromAsset(getAssets(),
                "toolBar.ttf");
        ojassTeam.setTypeface(tf);
        teamName= (TextView) CoreTeamActivity.this.findViewById(R.id.team_name);
        Typeface teamTypeName = Typeface.createFromAsset(getAssets(), "font3.ttf");
        teamName.setTypeface(teamTypeName);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.vp_main);
        viewPager.setAdapter(new MainPagerAdapter(getSupportFragmentManager()));
        viewPager.setOffscreenPageLimit(2);

    }


    public static void onHorizontalPageChanged(boolean increase) {
        if (increase)
        {
            ++currentPos;
        }
        else
            --currentPos;
        if (currentPos==teamNames.length||currentPos<0)
            currentPos=(currentPos+teamNames.length)%teamNames.length;
        teamName.setText(teamNames[currentPos]);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        currentPos=0;
    }
}
