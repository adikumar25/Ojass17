package com.parikshit.ojass.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.parikshit.ojass.R;
import com.parikshit.ojass.Utils.GoogleSigninService;
import com.parikshit.ojass.model.UserModel;

public class ProfileActivity extends AppCompatActivity {

    FirebaseAuth auth;
    public ProgressDialog p;
    EditText name,roll,branch,institute;
    DatabaseReference ref,ref2;
    String n,r,b,i;
    Long count;
    private static final String PREFS_TEXT="LoginInfo";
    private static final String ID="ProfileID";
    private static final String EMAIL="Email";
    private static final String PHOTOURL="photoUrl";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Button register= (Button) findViewById(R.id.btnLinkToRegisterScreen);
        auth=FirebaseAuth.getInstance();
        p=new ProgressDialog(this);
        p.setMessage("Please Wait....");
        p.setCancelable(false);
        p.show();
        name=(EditText)findViewById(R.id.name1);
        roll=(EditText)findViewById(R.id.roll1);
        branch=(EditText)findViewById(R.id.branch1);
        institute=(EditText)findViewById(R.id.institute1);
        ref= FirebaseDatabase.getInstance().getReference().child("users");
        ref.keepSynced(true);
        SharedPreferences sharedPreferences=this.getSharedPreferences(PREFS_TEXT,Context.MODE_PRIVATE);
        String Id=sharedPreferences.getString(ID,null);
        final String Email=sharedPreferences.getString(EMAIL,null);
        final String PhotoUrl=sharedPreferences.getString(PHOTOURL,null);

        ref.child(auth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    p.dismiss();
                    name.setText(dataSnapshot.child("name").getValue(String.class));
                    roll.setText(dataSnapshot.child("roll").getValue(String.class));
                    branch.setText(dataSnapshot.child("branch").getValue(String.class));
                    institute.setText(dataSnapshot.child("institute").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        register.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        n=name.getText().toString();
                        r=roll.getText().toString();
                        b=branch.getText().toString();
                        i=institute.getText().toString();
                        if(n.equals("")||r.equals("")||b.equals("")||i.equals(""))
                        {

                            Toast.makeText(getApplicationContext(),"All fields are required",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            ref.child(auth.getCurrentUser().getUid()).setValue(new UserModel(Email,PhotoUrl,i,b,r,n));
                           // ref.child(GoogleSigninService.account.getId()).setValue(new UserModel(userid,new Long(100),GoogleSigninService.account.getEmail(),GoogleSigninService.account.getPhotoUrl().toString(),i,b,r,n));

                           // firebaseAuthWithGoogle(GoogleSigninService.account);

                            Toast.makeText(getApplicationContext(),"Profile Updated",Toast.LENGTH_SHORT).show();


                            finish();

                        }






                    }
                }
        );

    }

    @Override
    public void onBackPressed() {

              finish();
    }
}
