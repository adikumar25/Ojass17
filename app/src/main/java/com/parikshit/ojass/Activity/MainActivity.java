package com.parikshit.ojass.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rubensousa.floatingtoolbar.FloatingToolbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.parikshit.ojass.Fragments.NavigationDrawerFragment;
import com.parikshit.ojass.Fragments.UmrellaEvents;
import com.parikshit.ojass.R;
import com.parikshit.ojass.model.CoordinatorsModel;
import com.parikshit.ojass.model.EventModel;
import com.parikshit.ojass.model.GuruGyanModel;
import com.parikshit.ojass.model.RulesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity implements FloatingToolbar.ItemClickListener{
    private static MainActivity mainActivity;
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawableToggle;
    Intent intent;
    private FloatingToolbar mFloatingToolbar;
    private TextView desc,title;
    private DatabaseReference ref,ref2,ref3,ref4;
    public static ArrayList<EventModel> data;
    FirebaseAuth auth;
    public ProgressDialog progressDialog;
    TextView userName,score;
    CircleImageView photo;
    ImageView subscribe,oye;
    public static ArrayList<GuruGyanModel> data1;

    private static final String PREFS_TEXT="LoginInfo";
    private static final String ID="ProfileID";
    private static final String EMAIL="Email";
    private static final String PHOTOURL="photoUrl";
    public static Long number;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 3;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ref= FirebaseDatabase.getInstance().getReference().child("events");
        ref.keepSynced(true);
        subscribe=(ImageView)findViewById(R.id.subscribe);
        //oye=(ImageView)findViewById(R.id.oye);
        auth=FirebaseAuth.getInstance();
        SharedPreferences sharedPreferences=this.getSharedPreferences(PREFS_TEXT,Context.MODE_PRIVATE);
        String Id=sharedPreferences.getString(ID,null);
        String Email=sharedPreferences.getString(EMAIL,null);
        String PhotoUrl=sharedPreferences.getString(PHOTOURL,null);
        ref2= FirebaseDatabase.getInstance().getReference().child("users").child(auth.getCurrentUser().getUid());
        ref2.keepSynced(true);
        ref3= FirebaseDatabase.getInstance().getReference().child("score").child(auth.getCurrentUser().getUid());
        ref3.keepSynced(true);
        data=new ArrayList<>();
        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().subscribeToTopic("OJASS");

        if((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M))
        {
            if(!checkPermissionForCamera())
            {
                requestPermissionForCamera();
            }
        }

        initToolbar();
        desc=(TextView)findViewById(R.id.description);
        title=(TextView)findViewById(R.id.title);
        userName=(TextView)findViewById(R.id.userName);
        score=(TextView)findViewById(R.id.score_text);
        photo=(CircleImageView)findViewById(R.id.profile_image);
        Typeface t=Typeface.createFromAsset(getAssets(),"Ojass.otf");
        //title.setTypeface(t);
        mDrawerLayout= (DrawerLayout) findViewById(R.id.drawerLayout);

        /*oye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = getOpenOyeIntent(getApplicationContext());
                startActivity(intent);
            }
        });
*/
        //Toast.makeText(getApplicationContext(), auth.getCurrentUser().getUid(), Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(), auth.getCurrentUser().getProviderId(), Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(), auth.getCurrentUser().getDisplayName(), Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(), auth.getCurrentUser().getEmail(), Toast.LENGTH_SHORT).show();

        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UmrellaEvents dialogFragment=new UmrellaEvents();
                dialogFragment.show(getSupportFragmentManager(),"Subscribe");
            }
        });


        Picasso.with(getApplicationContext()).load(auth.getCurrentUser().getPhotoUrl()).into(photo);

        String token= FirebaseInstanceId.getInstance().getToken();
        Log.d("qwerty","Token : "+token);
        //FirebaseMessaging.getInstance().subscribeToTopic("kalpIt");

        desc.setText("Ojass, in its 8th installment, promise to live up to its name, implying invigorating, boundless energy. The annual Techno-Management fest of NIT Jamshedpur is one of East India's biggest and grandest college festivals. Year after year, it has been evolving, reinventing itself in leaps and bounds. Every year, Ojass throws participants newer an unprecedented challenges, sets new standards, leaving the participants awestruck. It is no wonder that Ojass has gained the kind of recognition it has, in such a short time span.");


        ref4=FirebaseDatabase.getInstance().getReference().child("GuruGyan");
        ref4.keepSynced(true);

        data1=new ArrayList<>();
        ref4.child("number").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                 number=dataSnapshot.getValue(Long.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ref4.child("card data").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                data1.clear();
                for(DataSnapshot ds:dataSnapshot.getChildren())
                {
                    GuruGyanModel d=ds.getValue(GuruGyanModel.class);
                    data1.add(d);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ProfileActivity.class));
            }
        });



        ref2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
               // Toast.makeText(getApplicationContext(),dataSnapshot.child("name").getValue(String.class), Toast.LENGTH_SHORT).show();
                userName.setText(dataSnapshot.child("name").getValue(String.class));
                Picasso.with(getApplicationContext()).load(dataSnapshot.child("photoURL").getValue(String.class)).placeholder(R.drawable.drawer_avatar_circular).into(photo);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.exists())
                {
                    score.setText(dataSnapshot.getValue(String.class)+"");
                }
                else {
                    score.setText("100");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);
        mFloatingToolbar =(FloatingToolbar)findViewById(R.id.floatingToolbar);

        mFloatingToolbar.setClickListener(this);
        mFloatingToolbar.attachFab(fab);

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Initialising App data...");
        progressDialog.setCancelable(false);
        progressDialog.show();



        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                data.clear();
                progressDialog.dismiss();
                //Toast.makeText(getApplicationContext(), "DataFetched", Toast.LENGTH_SHORT).show();
                for(DataSnapshot ds: dataSnapshot.getChildren())
                {
                    String about=ds.child("about").getValue(String.class);
                    String branch=ds.child("branch").getValue(String.class);
                    String details=ds.child("detail").getValue(String.class);
                    String name=ds.child("name").getValue(String.class);
                    Long prize1=ds.child("prize").child("first").getValue(Long.class);
                    Long prize2=ds.child("prize").child("second").getValue(Long.class);
                    Long prize3=ds.child("prize").child("third").getValue(Long.class);
                    Long prizeT=ds.child("prize").child("total").getValue(Long.class);
                    ArrayList<CoordinatorsModel> coordinatorsModelArrayList=new ArrayList<>();
                    coordinatorsModelArrayList.clear();

                    ArrayList<RulesModel> rulesModelArrayList=new ArrayList<>();
                    rulesModelArrayList.clear();

                    for(DataSnapshot d:ds.child("coordinators").getChildren())
                    {
                        CoordinatorsModel coordinatorsModel=d.getValue(CoordinatorsModel.class);
                        coordinatorsModelArrayList.add(coordinatorsModel);
                    }

                    for(DataSnapshot d:ds.child("rules").getChildren())
                    {
                        RulesModel rulesModel=d.getValue(RulesModel.class);
                        rulesModelArrayList.add(rulesModel);
                    }


                    data.add(new EventModel(about,branch,details,name,prize1,prize2,prize3,prizeT,coordinatorsModelArrayList,rulesModelArrayList));




/*
                    Log.d("Event","About :"+about);
                    Log.d("Event","Branch :"+branch);
                    Log.d("Event","Details :"+details);
                    Log.d("Event","Name :"+name);
                    Log.d("Event","Prize1 :"+prize1);
                    Log.d("Event","Prize2 :"+prize2);
                    Log.d("Event","Prize3 :"+prize3);
                    Log.d("Event","PrizeT :"+prizeT);
                    for(int i=0;i<coordinatorsModelArrayList.size();i++)
                        Log.d("Event","Name :"+coordinatorsModelArrayList.get(i).getName()+"  Phone :"+coordinatorsModelArrayList.get(i).getPhone());

                    for(int i=0;i<rulesModelArrayList.size();i++)
                        Log.d("Event","Rules"+i+" :"+rulesModelArrayList.get(i).getText());*/

                   // showLog();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.navigation_indicator);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_appdrawer);
        drawerFragment.setUp((DrawerLayout) findViewById(R.id.drawerLayout), toolbar);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        drawerFragment.openDrawer();
                    }
                }
        );



    }

    @Override
    public void onItemClick(MenuItem item) {
        int id = item.getItemId();
        switch (id)
        {
            case R.id.facebook:
                intent = getOpenFacebookIntent(this);
                startActivity(intent);
                break;
            case R.id.instagram:
                intent = getOpenInstagramIntent(this);
                startActivity(intent);
                break;
            case R.id.youtube:
                intent = getOpenYouTubeIntent(this);
                startActivity(intent);
                break;
            case R.id.twitter:
                intent = getOpenTwitterIntent(this);
                startActivity(intent);
                break;
            case R.id.website:
                intent = getOpenWebsiteIntent(this);
                startActivity(intent);
                break;


        }
    }

    public static Intent getOpenFacebookIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://page/586755524720318"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/Ojassnitjamshedpur"));
        }
    }

    public static Intent getOpenTwitterIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.twitter.android", 0);
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/Ojass2k16"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/Ojass2k16"));
        }
    }

    public static Intent getOpenWebsiteIntent(Context context) {

        try {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://m.ojass.in"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://m.ojass.in"));
        }
    }
    public static Intent getOpenOyeIntent(Context context) {

        try {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://goo.gl/adyxXD"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://goo.gl/adyxXD"));
        }
    }

    public static Intent getOpenInstagramIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.instagram.android", 0);
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/_u/ojass_nitjsr"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/ojass_nitjsr"));
        }
    }

    public static Intent getOpenYouTubeIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.google.android.youtube", 0);
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.youtube.com/user/ojassnitjsr"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.youtube.com/user/ojassnitjsr")); //catches and opens a url to the desired page
        }
    }

    @Override
    public void onItemLongClick(MenuItem item) {

    }

    void showLog()
    {

        for(int i=0;i<data.size();i++)
        {
            Log.d("Event","About :"+data.get(i).getAbout());
            Log.d("Event","Branch :"+data.get(i).getBranch());
            Log.d("Event","Details :"+data.get(i).getDetails());
            Log.d("Event","Name :"+data.get(i).getName());
            Log.d("Event","Prize1 :"+data.get(i).getPrize1());
            Log.d("Event","Prize2 :"+data.get(i).getPrize2());
            Log.d("Event","Prize3 :"+data.get(i).getPrize3());
            Log.d("Event","PrizeT :"+data.get(i).getPrizeT());
            for(int j=0;j<data.get(i).getCoordinatorsModelArrayList().size();j++)
                Log.d("Event","Name :"+data.get(i).getCoordinatorsModelArrayList().get(j).getName()+"  Phone :"+data.get(i).getCoordinatorsModelArrayList().get(j).getPhone());

            for(int j=0;j<data.get(i).getRulesModels().size();j++)
                Log.d("Event","Rules"+i+" :"+data.get(i).getRulesModels().get(j).getText());
        }





    }

    @Override
    public void onBackPressed() {
        if (mFloatingToolbar.isShowing())
        {
            mFloatingToolbar.hide();
        }
        else
        {
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to exit?");
            builder.setNegativeButton("No",null);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    MainActivity.this.finish();


                }
            });
            builder.setCancelable(false);
            AlertDialog alertDialog=builder.create();
            alertDialog.show();

        }
    }

    public boolean checkPermissionForCamera(){
        int result = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    public void requestPermissionForCamera(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.CAMERA)){
            Toast.makeText(MainActivity.this, "Camera permission needed. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA},CAMERA_PERMISSION_REQUEST_CODE);
        }
    }
}

