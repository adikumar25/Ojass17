package com.parikshit.ojass.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.google.android.gms.common.SignInButton;
import com.google.firebase.auth.FirebaseAuth;
import com.parikshit.ojass.R;
import com.parikshit.ojass.Utils.GoogleSigninService;
import com.parikshit.ojass.adapter.OnboardingAdapter;

public class OnBoardingActivity extends FragmentActivity {

    GoogleSigninService googleSigninService;
    SignInButton register;
    FirebaseAuth auth;


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_on_boarding);
        final HorizontalInfiniteCycleViewPager horizontalInfiniteCycleViewPager =
                (HorizontalInfiniteCycleViewPager) findViewById(R.id.hicvp);
        horizontalInfiniteCycleViewPager.setAdapter(new OnboardingAdapter(OnBoardingActivity.this, false));
        register= (SignInButton) findViewById(R.id.normalButton);
        register.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        googleSigninService=new GoogleSigninService(OnBoardingActivity.this);
                        googleSigninService.login();
                    }
                }
        );

        auth=FirebaseAuth.getInstance();
        if(auth.getCurrentUser()!=null)
        {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       // Log.e("MainActivity","Activity Result called");
        googleSigninService.handleResult(requestCode, resultCode, data);

    }



}
