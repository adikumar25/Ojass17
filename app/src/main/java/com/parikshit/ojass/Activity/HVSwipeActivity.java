package com.parikshit.ojass.Activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;

import com.parikshit.ojass.R;

public class HVSwipeActivity extends AppCompatActivity {

    public static ViewPager viewPager;
    private float x1, y1, x2, y2;
    private int Duration = 200;
    private long startTime;
    private final int MIN_DISTANCE = 150;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hvswipe);

    //    viewPager = (ViewPager) findViewById(R.id.viewpager);
    //    viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
    //    viewPager.setOnTouchListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchEvent) {
        switch(touchEvent.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                // DOUBLE TAP -----------------------------------------------------------------------------
                if(System.currentTimeMillis() - startTime <= Duration) {
                //    Toast.makeText(this, "DOUBLE TAP !!", Toast.LENGTH_SHORT).show();
                }
                break;

            case MotionEvent.ACTION_UP:
                startTime = System.currentTimeMillis();
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();

                float deltaX = x2 - x1;
                float deltaY = y2 - y1;
                if (deltaX > MIN_DISTANCE)
                {   // SWIPE LEFT TO RIGHT ---------------------------------------------------------
                    Intent intent = new Intent(this, VerticalSwipeActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    finish();
                }
                else if( Math.abs(deltaX) > MIN_DISTANCE)
                {    // SWIPE RIGHT TO LEFT --------------------------------------------------------
                //    Intent intent = new Intent(this, ScrollViewActivity.class);
                //    startActivity(intent);
                //    finish();
                }
                else if(deltaY > MIN_DISTANCE){
                    // SWIPE TOP TO BOTTOM ---------------------------------------------------------

                }
                else if( Math.abs(deltaY) > MIN_DISTANCE){
                    // SWIPE BOTTOM TO TOP ---------------------------------------------------------

                }
                break;
        }
        return false;
    }
}