package com.parikshit.ojass.Utils;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Aditya on 02-02-2017.
 */

public class OjassContext extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        //Firebase.setAndroidContext(this);
    }
}
