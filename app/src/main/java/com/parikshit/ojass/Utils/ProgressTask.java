package com.parikshit.ojass.Utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.widget.ProgressBar;

/**
 * Created by Aditya on 13-02-2017.
 */

class ProgressTask extends AsyncTask<Integer, Integer, Void> {
    ProgressBar progress;
    Context context;
    public ProgressTask(ProgressBar progress, Context context) {
        this.progress = progress;
        this.context = context;

    }
    @Override
    protected void onPreExecute() {
        // initialize the progress bar
        // set maximum progress to 100.
        progress.setMax(100);

    }


    @Override
    protected Void doInBackground(Integer... params) {
        // get the initial starting value
        int start=params[0];


        // increment the progress
        for(int i=start;i<=100;i+=5){
            try {
                boolean cancelled=isCancelled();
                //if async task is not cancelled, update the progress
                if(!cancelled){
                    publishProgress(i);
                    SystemClock.sleep(1000);

                }

            } catch (Exception e) {
                Log.e("Error", e.toString());
            }

        }
        return null;
    }
    //Has direct connection to UI Main thread
    //Called everytime publishProgress(int) is called in doInBackground
    @Override
    protected void onProgressUpdate(Integer... values) {
        progress.setProgress(values[0]);
        //Toast.makeText(context, "test"+values[0], Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onPostExecute(Void result) {
        // async task finished
        Log.v("Progress", "Finished");
    }
    @Override
    protected void onCancelled() {
        progress.setMax(0);
    }
}
