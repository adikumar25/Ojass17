package com.parikshit.ojass.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.parikshit.ojass.Activity.MainActivity;
import com.parikshit.ojass.Activity.UserInfoActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by mayank2212 on 1/9/2017.
 */

public class GoogleSigninService {

    private final int RCSIGNIN = 9001;
    ProgressDialog p;

    private static GoogleApiClient mGoogleApiClient;
    private Context context;
    private Activity activity;
    private String KEY = "AIzaSyBzLEc-Wqxfv6CRxSlKO1zeKYG50TF6BNU";
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener mAuthListener;
    public static GoogleSignInAccount account;
    private static final String PREFS_TEXT="LoginInfo";
    private static final String ID="ProfileID";
    private static final String EMAIL="Email";
    private static final String PHOTOURL="photoUrl";
    DatabaseReference ref;


    public GoogleSigninService(Context context) {
        activity = (Activity) context;
        this.context = context;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("563048678217-36rulcdflddmr7hcd1r0qbepqhrsjn6u.apps.googleusercontent.com")
                .requestProfile()
                .requestEmail()
                .build();

        auth=FirebaseAuth.getInstance();
        p=new ProgressDialog(context);
        p.setMessage("Signing In....");
        ref= FirebaseDatabase.getInstance().getReference().child("users");
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
    }

    public void login() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        ((Activity) context).startActivityForResult(signInIntent, RCSIGNIN);
    }

    //for requesting permission in marshmallow
    public void requestPermission(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    ((Activity) context).startActivityForResult(signInIntent, RCSIGNIN);
                } else {
                   // Toast.makeText(context, "Login Permission Denied by the user", Toast.LENGTH_SHORT).show();
                }
        }
    }

    public void onStart() {
        mGoogleApiClient.connect();
    }

    public void handleResult(int requestCode, int resultCode, Intent data) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.e("GoogleSignInservice",result.getStatus().toString());
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticte with Firebase
                account = result.getSignInAccount();
                /*Toast.makeText(context, account.getId(),
                        Toast.LENGTH_SHORT).show();
                Toast.makeText(context, account.getIdToken(),
                        Toast.LENGTH_SHORT).show();*/
                firebaseAuthWithGoogle(account);
               // Toast.makeText(context, "Login Successful",Toast.LENGTH_SHORT).show();


                Log.e("GoogleSigninService","Login Successful");
            } else {
//                Toast.makeText(context, "Google Sign in Authentication failed.",
//                        Toast.LENGTH_SHORT).show();
               // Toast.makeText(context, result.getStatus().toString(),Toast.LENGTH_SHORT).show();
                Log.e("MainActivity","Google Sign in Authentication failed.");
            }
    }



    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
       // Log.d("Login", "firebaseAuthWithGoogle:" + acct.getIdToken());
        p.show();

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        auth.signInWithCredential(credential)
                .addOnCompleteListener((Activity)context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("Login", "signInWithCredential:onComplete:" + task.isSuccessful());


                        if(task.isSuccessful())
                        {

                            SharedPreferences sharedPreferences=context.getSharedPreferences(PREFS_TEXT, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor=sharedPreferences.edit();
                            editor.putString(ID,GoogleSigninService.account.getId());
                            editor.putString(EMAIL,GoogleSigninService.account.getEmail());
                            editor.putString(PHOTOURL,GoogleSigninService.account.getPhotoUrl().toString());
                            editor.apply();
                            ref.child(auth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    if(dataSnapshot.exists())
                                    {
                                        p.dismiss();
                                        Intent intent=new Intent(context,MainActivity.class);
                                        context.startActivity(intent);
                                        ((Activity)context).finish();
                                    }
                                    else
                                    {
                                        p.dismiss();
                                        Intent intent=new Intent(context,UserInfoActivity.class);
                                        context.startActivity(intent);
                                        ((Activity)context).finish();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    p.dismiss();
                                    //auth.signOut();
                                    //Toast.makeText(context,"Login failed... Please try after some time!!!",Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w("Login", "signInWithCredential", task.getException());
                            //Toast.makeText(context, "Authentication failed.",Toast.LENGTH_SHORT).show();
                            p.dismiss();
                            Toast.makeText(context,"Login failed... Please try after some time!!!",Toast.LENGTH_SHORT).show();
                       }

                    }
                });
    }



}
