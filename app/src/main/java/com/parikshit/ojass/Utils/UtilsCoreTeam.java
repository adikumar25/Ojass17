package com.parikshit.ojass.Utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.parikshit.ojass.R;

public class UtilsCoreTeam {

    public static void setupItem(final Context context, final View view, final LibraryObject libraryObject ) {


        //setting Name to the textView
        final TextView txt = (TextView) view.findViewById(R.id.textViewName);
        txt.setText(libraryObject.getTitle());
        Typeface tf1=Typeface.createFromAsset(context.getAssets(),"coreTeamFont.ttf");
        txt.setTypeface(tf1);
        TextView textViewPost = (TextView) view.findViewById(R.id.textViewPost);
        textViewPost.setText(libraryObject.getPost());
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "coreTeamFont.ttf");
        textViewPost.setTypeface(tf);
        final TextView mobile_no=(TextView)view.findViewById(R.id.mobile_no);
        //setting image to the textView

        mobile_no.setText(libraryObject.getMobileNo());
        mobile_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num = mobile_no.getText().toString().trim();
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + num));
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.e("CALL_INTENT", "CALL FAILED", e);
                }
            }
        });

        final ImageView img = (ImageView) view.findViewById(R.id.imageViewProfilepic);
        img.setImageResource(libraryObject.getRes());

    }

    public static class LibraryObject {

        private String mTitle;
        private String post;
        private int mRes;
        private String mobile_no;

        public LibraryObject( final String post,final int res, final String title,final String mobiile_no) {
            this.post = post;
            mRes = res;
            mTitle = title;
            this.mobile_no=mobiile_no;
        }

        public String getTitle() {
            return mTitle;
        }

        public int getRes() {
            return mRes;
        }

        public void setRes(final int res) {
            mRes = res;
        }

        public String getPost() {
            return post;
        }
        private String getMobileNo()
        {
            return  mobile_no;
        }
    }

}
