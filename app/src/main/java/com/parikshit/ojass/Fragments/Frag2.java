package com.parikshit.ojass.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parikshit.ojass.R;

public class Frag2 extends Fragment {


    public Frag2() {
        // Required empty public constructor
    }

    private TextView tv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frag2, container, false);
        tv = (TextView) v.findViewById(R.id.feed);
    //    tv.setText("Feed\n"+HRVAdapter.global_event);
        tv.setText("Fragment Two");
    //    Toast.makeText(getActivity(), "Fragment Two !!", Toast.LENGTH_SHORT).show();
        return v;
    }

}
